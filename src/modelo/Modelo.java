package modelo;

//@author Neto

import javax.swing.JOptionPane;
import vistas.*;


public class Modelo {
    public V_Entrada entrada;
    public V_Info info;
    public V_Salida salida;
    public V_Inventario inventario;
    public V_Libros libros;
    public V_Usuarios usuarios;

    public Modelo(V_Entrada entrada, V_Info info, V_Salida salida, V_Inventario inventario, V_Libros libros, V_Usuarios usuarios) {
        this.entrada = entrada;
        this.info = info;
        this.salida = salida;
        this.inventario = inventario;
        this.libros = libros;
        this.usuarios = usuarios;
    }
    
    public void entrada_aceptar(){
        if (entrada.tf1.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Nombre del Libro");
        } else if (entrada.tf3.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Nombre del Solicitante");
        } else{
            entrada.model.addRow(new Object[] { entrada.tf1.getText(),""+entrada.cb1.getSelectedItem()+"/"+entrada.cb2.getSelectedItem()+"/"
                    +entrada.cb3.getSelectedItem(), entrada.tf3.getText()});
            entrada.tf1.setText("");
            entrada.tf3.setText("");
            entrada.cb1.setSelectedIndex(0);
            entrada.cb2.setSelectedIndex(0);
            entrada.cb3.setSelectedIndex(0);
            JOptionPane.showMessageDialog(null, "Registro Exitoso");
        }
    }
    
    public void salida_aceptar(){
        if (salida.tf1.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Nombre del Libro");
        } else if (salida.tf3.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Nombre del Solicitante");
        } else{
            salida.model.addRow(new Object[] { salida.tf1.getText(),""+salida.cb.getSelectedItem()+"/"+salida.cb2.getSelectedItem()+"/"
                    +salida.cb3.getSelectedItem(), salida.tf3.getText()});
            salida.tf1.setText("");
            salida.tf3.setText("");
            salida.cb.setSelectedIndex(0);
            salida.cb2.setSelectedIndex(0);
            salida.cb3.setSelectedIndex(0);
            JOptionPane.showMessageDialog(null, "Registro Exitoso");
        }
    }
    
    public void inventario_eliminar(){
        inventario.model.removeRow(inventario.table.getSelectedRow());
    }
    
    public void libros_registrar(){
        if (libros.tf1.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Autor del Libro");
        } else if (libros.tf2.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Titulo de Libro");
        }else if (libros.tf3.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Clave del Libro");
        }
        else if (libros.tf4.getText().contentEquals("")){   
            JOptionPane.showMessageDialog(null,"Ingresar Tipo de Texto");}
        else{
            String [] datos= new String[4];
            datos[0]=libros.tf1.getText();
            libros.tf1.setText(null);
            datos[1]=libros.tf2.getText();
            libros.tf2.setText(null);
            datos[2]=libros.tf3.getText();
            libros.tf3.setText(null);
            datos[3]=libros.tf4.getText();
            libros.tf4.setText(null);
            libros.model.addRow(datos);
            inventario.model.addRow(datos);
            JOptionPane.showMessageDialog(null, "Registro Exitoso");
        }
    }
    
    public void usuarios_guardar(){
        if (usuarios.tf1.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar ID");
        } else if (usuarios.tf2.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Nombre ");
        }  else if(usuarios.tf3.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Direccion ");
        } else if (usuarios.tf4.getText().contentEquals("")){
            JOptionPane.showMessageDialog(null,"Ingresar Telefono ");
        } else{
            usuarios.model.addRow(new Object[] { usuarios.tf1.getText(),usuarios.tf2.getText(), usuarios.tf3.getText(),usuarios.tf4.getText()});
            usuarios.tf1.setText("");
            usuarios.tf2.setText("");
            usuarios.tf3.setText("");
            usuarios.tf4.setText("");
            JOptionPane.showMessageDialog(null, "Registro Exitoso");
        }
    }
    
    public void usuarios_buscar(){
        String [] datos= new String[4];
        boolean si = false;
        for(int i = 0; i < usuarios.table.getRowCount(); i++){
        for(int j = 0; j < usuarios.table.getColumnCount(); j++){
            if(usuarios.table.getModel().getValueAt(i, j).equals(usuarios.tf1.getText())){
                //JOptionPane.showMessageDialog(null, "ID: "+ usuarios.table.getModel().getValueAt(i, j));
                datos [0] = "ID:"+usuarios.table.getModel().getValueAt(i, 0);
                datos [1] = "Nombre:"+usuarios.table.getModel().getValueAt(i, 1);
                datos [2] = "Direccion:"+usuarios.table.getModel().getValueAt(i, 2);
                datos [3] = "Telefono:"+usuarios.table.getModel().getValueAt(i, 3);
                JOptionPane.showMessageDialog(null, datos);
                si = true;
            }
        }
        } 
        if (si == false)
            JOptionPane.showMessageDialog(null, "No existe");
    }

}
