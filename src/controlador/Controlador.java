package controlador;

//@author Neto

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import modelo.Modelo;
import vistas.V_Entrada;
import vistas.V_Info;
import vistas.V_Inventario;
import vistas.V_Libros;
import vistas.V_Salida;
import vistas.V_Usuarios;


public class Controlador implements ActionListener{
    public V_Entrada entrada;
    public V_Info info;
    public V_Salida salida;
    public V_Inventario inventario;
    public V_Libros libros;
    public V_Usuarios usuarios;
    public Modelo modelo;

    public Controlador(V_Entrada entrada, V_Info info, V_Salida salida, V_Inventario inventario, V_Libros libros, V_Usuarios usuarios, Modelo modelo) {
        this.entrada = entrada;
        this.info = info;
        this.salida = salida;
        this.inventario = inventario;
        this.libros = libros;
        this.usuarios = usuarios;
        this.modelo = modelo;
        
        this.entrada.aceptar.addActionListener(this);
        this.entrada.salir.addActionListener(this);
        this.entrada.ver.addActionListener(this);
        this.info.aceptar.addActionListener(this);
        this.salida.aceptar.addActionListener(this);
        this.salida.cancelar.addActionListener(this);
        this.salida.registro.addActionListener(this);
        this.inventario.aceptar.addActionListener(this);
        this.inventario.añadir.addActionListener(this);
        this.inventario.eliminar.addActionListener(this);
        this.libros.cancelar.addActionListener(this);
        this.libros.ver.addActionListener(this);
        this.libros.registrar.addActionListener(this);
        this.usuarios.buscar.addActionListener(this);
        this.usuarios.guardar.addActionListener(this);
        this.usuarios.salir.addActionListener(this);
    }
    
    public void iniciarentrada() {
        entrada.setTitle("Entrada de Libros");
        entrada.pack();
        entrada.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        entrada.setLocationRelativeTo(null);
        entrada.setVisible(true);  
    }
    
    public void iniciarinfo() {
        info.setTitle("Acerca de");
        info.pack();
        info.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        info.setLocationRelativeTo(null);
        info.setVisible(true);  
    }
    public void iniciarsalida() {
        salida.setTitle("Salida de Libros");
        salida.pack();
        salida.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        salida.setLocationRelativeTo(null);
        salida.setVisible(true);  
    }
    public void iniciarinventario() {
        inventario.setTitle("Inventario");
        inventario.pack();
        inventario.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        inventario.setLocationRelativeTo(null);
        inventario.setVisible(true);  
    }
    public void iniciarlibros() {
        libros.setTitle("Libros Nuevos");
        libros.pack();
        libros.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        libros.setLocationRelativeTo(null);
        libros.setVisible(true);  
    }
    public void iniciarusuario() {
        usuarios.setTitle("Usuarios");
        usuarios.pack();
        usuarios.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        usuarios.setLocationRelativeTo(null);
        usuarios.setVisible(true);  
    }
    
    public void actionPerformed(ActionEvent evento){
        //Entrada
        if(entrada.aceptar == evento.getSource()){
            modelo.entrada_aceptar();
        }
        if(entrada.salir == evento.getSource()){
            entrada.setVisible(false);
        }
        if(entrada.ver == evento.getSource()){
            entrada.f.setVisible(true);
        }
        
        //Info
        if(info.aceptar == evento.getSource()){
            info.setVisible(false);
        }
        
        //Salida
        if(salida.aceptar == evento.getSource()){
            modelo.salida_aceptar();
        }
        if(salida.cancelar == evento.getSource()){
            salida.setVisible(false);
        }
        if(salida.registro == evento.getSource()){
            salida.f.setVisible(true);
        }
        
        //Inventario
        if(inventario.aceptar == evento.getSource()){
            inventario.setVisible(false);
        }
        if(inventario.añadir == evento.getSource()){
            iniciarlibros();
        }
        if(inventario.eliminar == evento.getSource()){
            modelo.inventario_eliminar();
        }
        
        //Libros
        if(libros.ver == evento.getSource()){
            libros.f.setVisible(true);
        }
        if(libros.registrar == evento.getSource()){
            modelo.libros_registrar();
        }
        if(libros.cancelar == evento.getSource()){
            libros.setVisible(false);
        }
        
        //Usuario
        if(usuarios.buscar == evento.getSource()){
            modelo.usuarios_buscar();
        }
        if(usuarios.salir == evento.getSource()){
            usuarios.setVisible(false);
        }
        if(usuarios.guardar == evento.getSource()){
            modelo.usuarios_guardar();
        }
    }
    

}
