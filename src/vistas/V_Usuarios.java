package vistas;

//@author Neto

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class V_Usuarios extends JFrame{
    public JPanel panelMayor, panelCentro, panelSur;
    public JTextField tf1, tf2, tf3, tf4;
    public JLabel lbl1, lbl2, lbl3, lbl4;
    public JButton buscar, guardar, salir;
    public DefaultTableModel model = new DefaultTableModel();
    public JTable table = new JTable(model);
    
    public V_Usuarios(){
        
        panelMayor = new JPanel(new BorderLayout());
        panelCentro = new JPanel();
        panelSur = new JPanel();
        
        GridBagLayout gb = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        panelCentro.setLayout(gb);
        
        FlowLayout f2 = new FlowLayout();
        panelSur.setLayout(f2);
        
        lbl1 = new JLabel ("ID Usuario");
        tf1 = new JTextField (25);
        
        lbl2 = new JLabel ("Nombre");
        tf2 = new JTextField (25);
        
        lbl3 = new JLabel ("Direccion");
        tf3 = new JTextField (25);
        
        lbl4 = new JLabel ("Telefono");
        tf4 = new JTextField (25);
        
        
        
        buscar = new JButton ("Buscar por ID"); 
        
        guardar = new JButton ("Registrar");
        
        salir = new JButton ("Cancelar");
        
        gbc.gridx = 0;
        gbc.gridy = 0;
       
       panelCentro.add(lbl1,gbc);
       gbc.gridy = 1;
       gbc.gridwidth =5;
       panelCentro.add(tf1,gbc);
       gbc.gridwidth = 1;
       gbc.gridy = 2;
        
       panelCentro.add(lbl2,gbc);
       gbc.gridy = 3;
       gbc.gridwidth = 5;
       panelCentro.add(tf2,gbc);
       gbc.gridwidth = 1;
       gbc.gridy = 4;
       
       panelCentro.add(lbl3,gbc);
       gbc.gridy = 5;
       gbc.gridwidth = 5;
       panelCentro.add(tf3,gbc);
       gbc.gridwidth = 1;
       gbc.gridy = 6;
       
       panelCentro.add(lbl4,gbc);
       gbc.gridy = 7;
       gbc.gridwidth = 5;
       
       panelCentro.add(tf4,gbc);
       gbc.gridwidth = 1;
       gbc.gridy = 8;
       
       panelSur.add(buscar);
       panelSur.add(guardar);
       panelSur.add(salir);
       
       
     
       panelMayor.add(panelCentro,BorderLayout.CENTER);
       panelMayor.add(panelSur,BorderLayout.SOUTH);
    
       this.add(panelMayor);
       
       
       model.addColumn("ID Usuario");
       model.addColumn("Nombre");
       model.addColumn("Direccion");
       model.addColumn("Telefono");
       
        JFrame f = new JFrame();
        f.setSize(350,350);
        f.setLocation(350, 350);
        f.add(new JScrollPane(table));
        f.setVisible(false);
    }
}
