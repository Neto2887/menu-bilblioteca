package vistas;

//@author Neto

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class V_Entrada extends JFrame{
    public JButton aceptar,ver,salir;
    public JTextField tf1,tf2,tf3;
    public JComboBox cb1,cb2,cb3;
    public JList lista;
    public DefaultTableModel model = new DefaultTableModel();
    public JTable table = new JTable(model);
    public JFrame f = new JFrame();
    
    /*public V_Entrada(){
        marco();
    }*/
    
    public V_Entrada(){
       /* this.setSize(500, 500);
        this.setLocation(800, 100);
        this.setVisible(true);
        this.setTitle("Entrada de Libros");
//        */
        JPanel panelMayor = new JPanel(new BorderLayout());
        
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur = new JPanel();
        
        
        FlowLayout f1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(f1);
        
        GridBagLayout gb = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        panelCentro.setLayout(gb);
        
        FlowLayout f2 = new FlowLayout();
        panelSur.setLayout(f2);
        
        
        
        JLabel titulo = new JLabel("Registro de entrada de libros");
        
        JLabel lbl1 = new JLabel("Nombre del libro: ");
        tf1 = new JTextField(25);
       /* JLabel lbl2 = new JLabel("Numero de serie: ");
        tf2 = new JTextField(25);*/
        JLabel lbl3 = new JLabel("Nombre del Usuario: ");
        tf3 = new JTextField(25);
        
        String[] dia = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18",
                        "19","20","21","22","23","24","25","26","27","28","29","30","31"};
        JLabel lbl4 = new JLabel("Fecha:");
        cb1 = new JComboBox(dia);
        String[] mes = {"ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
        cb2 = new JComboBox(mes);
        String[] año = {"2010","2011","2012","2013","2014","2015","2016","2017"};
        cb3 = new JComboBox(año);
        
        aceptar = new JButton("Aceptar");
        ver = new JButton("Ver Registro");
        salir  = new JButton("Salir");
        
        panelNorte.add(titulo);
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        
        gbc.anchor = GridBagConstraints.WEST;
        panelCentro.add(lbl1,gbc);
        gbc.gridy = 1;
        gbc.gridwidth = 5;
        panelCentro.add(tf1,gbc);
        gbc.gridwidth = 1;
        gbc.gridy = 2;
        /*panelCentro.add(lbl2,gbc);
        gbc.gridy = 3;
        gbc.gridwidth = 5;
        panelCentro.add(tf2,gbc);
        gbc.gridwidth = 1;
        gbc.gridy = 4;*/
        panelCentro.add(lbl3,gbc);
        gbc.gridy = 5;
        gbc.gridwidth = 5;
        panelCentro.add(tf3,gbc);
        gbc.gridwidth = 1;
        gbc.gridy = 6;
        
        panelCentro.add(lbl4,gbc);
        gbc.gridy = 7;
        gbc.gridwidth = 5;
        panelCentro.add(cb1,gbc);
        gbc.gridwidth = 1;
        gbc.gridy = 7;
        gbc.gridx = 1;
        panelCentro.add(cb2,gbc);
        gbc.gridy = 7;
        gbc.gridwidth = 1;
        gbc.gridx = 2;
        panelCentro.add(cb3,gbc);
        gbc.gridy = 7;
        gbc.gridwidth = 1;
        gbc.gridx = 2;
       
               
        panelSur.add(aceptar);
        panelSur.add(ver);
        panelSur.add(salir);
        
        panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);
       
        
        this.add(panelMayor);
        
        model.addColumn("Titulo de libro");
        model.addColumn("Fecha que se entrego el libro");
        model.addColumn("Usuario");
        
        
        
        f.setSize(350,350);
        f.setLocation(350, 350);
        f.add(new JScrollPane(table));
        f.setVisible(false);
    }
}
