package vistas;

//@author Neto

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class V_Salida extends JFrame{
    public JButton aceptar,cancelar,registro;
    DefaultListModel lista = new DefaultListModel();
    public JTextField tf1,tf2,tf3;
    public JComboBox cb,cb2,cb3;
    public JList list;
    public DefaultTableModel model = new DefaultTableModel();
    public JTable table = new JTable(model);
    public JFrame f = new JFrame();
    
//    public V_Salida(){
//        marco();
//    }
    
    public V_Salida(){
        JPanel panelM = new JPanel(new BorderLayout());
        
        JPanel panelN = new JPanel();
        JPanel panelC = new JPanel();
        JPanel panelS = new JPanel();
        //JPanel panelO = new JPanel();
        
        FlowLayout f1 = new FlowLayout(FlowLayout.CENTER);
        panelN.setLayout(f1);
        
        GridBagLayout gb = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        panelC.setLayout(gb);
        
        FlowLayout f2 = new FlowLayout();
        panelS.setLayout(f2);
        
        JLabel titulo = new JLabel("Registro de salidas de libros");
        
        JLabel lbl1 = new JLabel("Titulo del libro: ");
        tf1 = new JTextField(25);
        /*JLabel lbl2 = new JLabel("Numero de serie: ");
        tf2 = new JTextField(25);*/
        JLabel lbl3 = new JLabel("Nombre del solicitante: ");
        tf3 = new JTextField(25);
        
        String[] dia = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18",
                        "19","20","21","22","23","24","25","26","27","28","29","30","31"};
        JLabel lbl4 = new JLabel("Fecha del dia:");
        cb = new JComboBox(dia);
        String[] mes = {"ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
        cb2 = new JComboBox(mes);
        String[] año = {"2010","2011","2012","2013","2014","2015","2016","2017"};
        cb3 = new JComboBox(año);
        
        aceptar = new JButton("Aceptar");
        cancelar = new JButton("Salir");
        registro = new JButton("Ver Registros de Salida");
        
        panelN.add(titulo);
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        
        gbc.anchor = GridBagConstraints.WEST;
        panelC.add(lbl1,gbc);
        gbc.gridy = 1;
        gbc.gridwidth = 5;
        panelC.add(tf1,gbc);
        gbc.gridwidth = 1;
        gbc.gridy = 2;
        /*panelC.add(lbl2,gbc);
        gbc.gridy = 3;
        gbc.gridwidth = 5;
        panelC.add(tf2,gbc);
        gbc.gridwidth = 1;
        gbc.gridy = 4;*/
        panelC.add(lbl3,gbc);
        gbc.gridy = 5;
        gbc.gridwidth = 5;
        panelC.add(tf3,gbc);
        gbc.gridwidth = 1;
        gbc.gridy = 6;
        
        panelC.add(lbl4,gbc);
        gbc.gridy = 7;
        gbc.gridwidth = 5;
        panelC.add(cb,gbc);
        gbc.gridwidth = 1;
        gbc.gridy = 7;
        gbc.gridx = 1;
        panelC.add(cb2,gbc);
        gbc.gridy = 7;
        gbc.gridwidth = 1;
        gbc.gridx = 2;
        panelC.add(cb3,gbc);
        gbc.gridy = 7;
        gbc.gridwidth = 1;
        gbc.gridx = 2;
        
        /*list = new JList();
        String[] info={"Libro:","Serie:","Solicitante:","Fecha:"};
        list.setListData(info);*/
        
        //panelO.add(list);
        
        panelS.add(aceptar);
        panelS.add(cancelar);
        panelS.add(registro);
        
        panelM.add(panelN, BorderLayout.NORTH);
        panelM.add(panelC, BorderLayout.CENTER);
        panelM.add(panelS, BorderLayout.SOUTH);
        //panelM.add(panelO, BorderLayout.EAST);
        
        this.add(panelM);
        
        model.addColumn("Titulo de libro");
        model.addColumn("Fecha que se saco");
        model.addColumn("Solicitante");

        
        f.setSize(350,350);
        f.setLocation(350, 350);
        f.add(new JScrollPane(table));
        f.setVisible(false);

    }
}
