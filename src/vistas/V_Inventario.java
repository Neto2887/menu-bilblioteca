package vistas;

//@author Neto

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;


public class V_Inventario extends JFrame{
    public JPanel c,m,s;
    public JButton aceptar,añadir,eliminar;
    public DefaultTableModel model = new DefaultTableModel();
    public JTable table = new JTable(model);
    
    
    public V_Inventario(){
        m = new JPanel(new BorderLayout());
        
        c = new JPanel();
        FlowLayout f1 = new FlowLayout();
        c.setLayout(f1);
        
        s = new JPanel();
        FlowLayout f2 = new FlowLayout();
        s.setLayout(f2);
        
        aceptar = new JButton("Aceptar");
        añadir = new JButton("Añadir");
        eliminar = new JButton("Eliminar");
        
        table.setShowGrid(true);
        model.addColumn("Autor");
        model.addColumn("Titulo");
        model.addColumn("Clave");
        model.addColumn("Texto");
        
        c.add(new JScrollPane(table));
        s.add(aceptar);
        s.add(añadir);
        s.add(eliminar);
        
        m.add(c, BorderLayout.NORTH);
        m.add(s, BorderLayout.SOUTH);
        
        this.add(m);
      
    }
}
