package vistas;

//@author Neto

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class V_Info extends JFrame{
    public JPanel pc,pm,ps;
    public JButton aceptar;
    public JLabel creo;
    
    public V_Info(){
        
        creo = new JLabel("Creado Por:\n"
                + "Ernesto Alvarez Calderon\n"
                + "Luis Eduardo Cavanzo Floriano\n"
                + "Margarito Hrnandez Garcia\n"
                + "Ver. 1000.1000.1000");
        
        aceptar = new JButton("Aceptar");
        
        pm = new JPanel(new BorderLayout());
        
        pc = new JPanel();
        ps = new JPanel();
        
        
        FlowLayout f1 = new FlowLayout(FlowLayout.CENTER);
        pc.setLayout(f1);
        
        FlowLayout f2 = new FlowLayout();
        ps.setLayout(f2);
       
        ps.add(aceptar);
        pc.add(creo);
        
        pm.add(pc, BorderLayout.NORTH);
        pm.add(ps, BorderLayout.SOUTH);        
        
        this.add(pm);
    }
}
