package vistas;

//@author Neto

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class V_Libros extends JFrame{
    
    public JPanel panelMayor, panelNorte, panelCentro, panelSur;
    public JLabel lbl1, lbl2, lbl3, lbl4;
    public JTextField tf1, tf2, tf3, tf4;
    public JButton registrar, cancelar,ver;
    public DefaultTableModel model = new DefaultTableModel();
    public JTable table = new JTable(model);
    public JFrame f = new JFrame();

    
    public V_Libros(){
        
        panelMayor = new JPanel(new BorderLayout());
        panelNorte = new JPanel();
        panelSur =new JPanel();
       
       GridBagLayout gbl1=new GridBagLayout();
       GridBagConstraints gbc1=new GridBagConstraints();
       panelNorte.setLayout(gbl1);
       
       FlowLayout fl2=new  FlowLayout();
       panelSur.setLayout(fl2);
      
      lbl1 = new JLabel("Autor del libro:");
      tf1 = new JTextField(25);
      lbl2 = new JLabel("Titulo de Libro:");
      tf2 = new JTextField(25);
      lbl3 = new JLabel("Clave del Libro:");
      tf3 = new JTextField(25);
      lbl4 = new JLabel("Tipo de texto");
      tf4 = new JTextField(25);
        
       registrar=new JButton();
       cancelar=new JButton();
       ver = new JButton("Ver Registros");
       registrar.setText("REGISTRAR");
   
        
       cancelar.setText("CANCELAR");
   
       
      gbc1.gridx = 0;
      gbc1.gridy = 0;
      
      gbc1.anchor = GridBagConstraints.WEST;
      panelNorte.add(lbl1,gbc1);
      gbc1.gridy =1;
      gbc1.gridwidth = 5;
      panelNorte.add(tf1,gbc1);
      gbc1.gridwidth = 1;
      gbc1.gridy = 2;
      panelNorte.add(lbl2,gbc1);
      gbc1.gridy=3;
      gbc1.gridwidth=5;
      panelNorte.add(tf2,gbc1);
      gbc1.gridwidth=1;
      gbc1.gridy=4;
      panelNorte.add(lbl3,gbc1);
      gbc1.gridwidth=5;
      gbc1.gridy=5;
      panelNorte.add(tf3,gbc1);
      gbc1.gridwidth=1;
      gbc1.gridy=6;
      panelNorte.add(lbl4,gbc1);
      gbc1.gridwidth=2;
      gbc1.gridy=7;
      panelNorte.add(tf4,gbc1);
    
    
      panelSur.add(registrar);
      panelSur.add(cancelar);
      panelSur.add(ver);
      
       panelMayor.add(panelNorte,BorderLayout.NORTH);
//       panelMayor.add(panelCentro);
       panelMayor.add(panelSur,BorderLayout.SOUTH);
       
       this.add(panelMayor);
       
      table.setShowGrid(true);
      model.addColumn("Autor");
      model.addColumn("Titulo");
      model.addColumn("Clave");
      model.addColumn("Texto");
      
        
        f.setSize(350,350);
        f.setLocation(350, 350);
        f.add(new JScrollPane(table));
        f.setVisible(false);
       
    
    }
}
